﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Net.Http;
using System.Web;
using System.Data.SqlClient;

namespace Twitter
{

    class TwitterData
    {
        static string constr = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=C:\Users\Hp Envy 14\Documents\Visual Studio 2013\Projects\Twitter\Twitter\TwitterRecord.mdf;Integrated Security=True";
        public string OAuthConsumerSecret { get; set; }
        public string OAuthConsumerKey { get; set; }

        public async Task<IEnumerable<string>> GetTwitts(string userName, int count, string accessToken = null)
        {
            if (accessToken == null)
            {
                accessToken = await GetAccessToken();
            }

            var requestUserTimeline = new HttpRequestMessage(HttpMethod.Get, string.Format("https://api.twitter.com/1.1/statuses/user_timeline.json?count={0}&screen_name={1}&trim_user=1&exclude_replies=1", count, userName));
           
            requestUserTimeline.Headers.Add("Authorization", "Bearer " + accessToken);
            var httpClient = new HttpClient();
            HttpResponseMessage responseUserTimeLine = await httpClient.SendAsync(requestUserTimeline);
            var serializer = new JavaScriptSerializer();
            dynamic json = serializer.Deserialize<object>(await responseUserTimeLine.Content.ReadAsStringAsync());
            
            var enumerableTwitts = (json as IEnumerable<dynamic>);

            if (enumerableTwitts == null)
            {
                return null;
            }
            return enumerableTwitts.Select(t => (string)(t["text"].ToString()));
        }


        public async Task<IEnumerable<string>> GetGlobalTrends(int LocationId, string accessToken = null)
        {
            if (accessToken == null)
            {
                accessToken = await GetAccessToken();
            }

            var requestUserTimeline = new HttpRequestMessage(HttpMethod.Get, string.Format("https://api.twitter.com/1.1/trends/place.json?id={0}",LocationId));

            requestUserTimeline.Headers.Add("Authorization", "Bearer " + accessToken);
            var httpClient = new HttpClient();
            HttpResponseMessage responseUserTimeLine = await httpClient.SendAsync(requestUserTimeline);
            var serializer = new JavaScriptSerializer();
            dynamic json = serializer.Deserialize<object>(await responseUserTimeLine.Content.ReadAsStringAsync());
            List<object> lst = new List<object>();
            foreach (var kvp in json)
            {

                foreach (var obj in kvp)
                {
                    foreach(var kpv1 in obj.Value)
                    {
                        lst.Add(kpv1);
                    }

                    break;
                }

                break;
            }

            var enumerableTwitts = (lst as IEnumerable<dynamic>);

            if (enumerableTwitts == null)
            {
                return null;
            }
            return enumerableTwitts.Select(t => (string)(t["name"].ToString()));
        }

        public async Task<IEnumerable<string>> GetTwitts_HashTags(string userName, string accessToken = null)
        {
            if (accessToken == null)
            {
                accessToken = await GetAccessToken();
            }
            var requestUserTimeline = new HttpRequestMessage(HttpMethod.Get, string.Format("https://api.twitter.com/1.1/search/tweets.json?q=%23{0}&count=190&since_id=24012619984051000", userName));
          
            requestUserTimeline.Headers.Add("Authorization", "Bearer " + accessToken);
            var httpClient = new HttpClient();
            HttpResponseMessage responseUserTimeLine = await httpClient.SendAsync(requestUserTimeline);
            var serializer = new JavaScriptSerializer();
            List<Object> lst = new List<object>();
            dynamic json = serializer.Deserialize<object>(await responseUserTimeLine.Content.ReadAsStringAsync());
             foreach (var kvp in json)
             {
                 
                     foreach(var obj in kvp.Value)
                     {
                         lst.Add(obj);
                     }
                 
                 break;
             }
            
            var enumerableTwitts = (lst as IEnumerable<dynamic>);

            if (enumerableTwitts == null)
            {
                return null;
            }
            return enumerableTwitts.Select(t => (string)(t["text"].ToString()));
        }

        public async Task<IEnumerable<string>> GetReTwitts(string userName, string accessToken = null)
        {
            if (accessToken == null)
            {
                accessToken = await GetAccessToken();
            }
            var requestUserTimeline = new HttpRequestMessage(HttpMethod.Get, string.Format("https://api.twitter.com/1.1/search/tweets.json?q=%23{0}&count=1&since_id=24012619984051000", userName));
           
            requestUserTimeline.Headers.Add("Authorization", "Bearer " + accessToken);
            var httpClient = new HttpClient();
            HttpResponseMessage responseUserTimeLine = await httpClient.SendAsync(requestUserTimeline);
            var serializer = new JavaScriptSerializer();
            List<Object> lst = new List<object>();
            dynamic json = serializer.Deserialize<object>(await responseUserTimeLine.Content.ReadAsStringAsync());
            foreach (var kvp in json)
            {

                foreach (var obj in kvp.Value)
                {
                    lst.Add(obj);
                }

                break;
            }
           
            var enumerableTwitts = (lst as IEnumerable<dynamic>);

            
            IEnumerable<string> id=enumerableTwitts.Select(t => (string)(t["id"].ToString()));
            List<string> Tweet_id = id.ToList();
            string TweetId = Tweet_id[0];
            var requestUserTimeline1 = new HttpRequestMessage(HttpMethod.Get, string.Format("https://api.twitter.com/1.1/statuses/retweets/%3{0}.json",TweetId));
           
            requestUserTimeline1.Headers.Add("Authorization", "Bearer " + accessToken);
            //var httpClient = new HttpClient();
            HttpResponseMessage responseUserTimeLine1 = await httpClient.SendAsync(requestUserTimeline1);
            //var serializer = new JavaScriptSerializer();
            List<Object> lst1 = new List<object>();
            dynamic json1 = serializer.Deserialize<object>(await responseUserTimeLine1.Content.ReadAsStringAsync());
            var enumerableReTwitts = (json1 as IEnumerable<dynamic>);
            
           return enumerableReTwitts.Select(t => (string)(t["id"].ToString()));
        }


        
        public async Task<string> GetAccessToken()
        {
            var httpClient = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Post, "https://api.twitter.com/oauth2/token ");
           // Console.WriteLine(request);
            OAuthConsumerKey = "HfSKXt71Xcz2izFQQjJpqDB2s";
            OAuthConsumerSecret = "la8BJwvwoJG69WuWPgO8B4tRn6kusPxtZQ316yGFn4uEbVsBgO";
            var customerInfo = Convert.ToBase64String(new UTF8Encoding().GetBytes(OAuthConsumerKey + ":" + OAuthConsumerSecret));
            //Console.WriteLine(customerInfo);
            request.Headers.Add("Authorization", "Basic " + customerInfo);
            request.Content = new StringContent("grant_type=client_credentials", Encoding.UTF8, "application/x-www-form-urlencoded");

            HttpResponseMessage response = await httpClient.SendAsync(request);

            string json = await response.Content.ReadAsStringAsync();
            var serializer = new JavaScriptSerializer();
            dynamic item = serializer.Deserialize<object>(json);
            return item["access_token"];
        }

        public static int MainMenu()
        {
            Console.WriteLine("-----------------------------------Menu-----------------------------------");
            Console.WriteLine();
            Console.WriteLine("Press 1 for Getting the tweets of a Specific User");
            Console.WriteLine("Press 2 for Getting the Top 100 tweets of a Specific Hash Tag");
            Console.WriteLine("Press 3 for Getting the Retweet Count of a Specific HashTag");
            Console.WriteLine("Press 4 for Getting the Top 10 Trends of a Place");
            Console.WriteLine("Press 5 to Exit");
            Console.WriteLine();
            Console.WriteLine("-----------------------------------Menu-----------------------------------");
            Console.WriteLine();
            int Choice = int.Parse(Console.ReadLine());
            if (Choice > 4)
            {
                while (Choice > 4)
                {
                    Console.WriteLine("You Entered wrong choice Please Enter a valid one");
                    Choice = int.Parse(Console.ReadLine());
                }
            }

           
            return Choice;
        }

        public static void add_UserTweets(String Username,string Tweets)
        {

            try
            {
                SqlConnection conn = new SqlConnection(constr);
                conn.Open();
                string q = "insert into UserTweets(UserName,Tweets) values( '"  + Username + "'" + "," + "'" + Tweets + "'"  + ")";
                //Console.WriteLine(q);
                SqlCommand cmd = new SqlCommand(q, conn);
                cmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine("exception caught" + e);
            }
        }

        public static void add_HashTags(String HashTag, string Tweets)
        {

            try
            {
                SqlConnection conn = new SqlConnection(constr);
                conn.Open();
                string q = "insert into HashTagTweets(HashTag,Tweet) values( '" + HashTag + "'" + "," + "'" + Tweets + "'" + ")";
                //Console.WriteLine(q);
                SqlCommand cmd = new SqlCommand(q, conn);
                cmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception e)
            {
                
            }
        }

        public static void add_Trends(int Loc_id, string Trends)
        {

            try
            {
                SqlConnection conn = new SqlConnection(constr);
                conn.Open();
                string q = "insert into Trends(Location_id,Trends) values( " + Loc_id  + "," + "'" + Trends + "'" + ")";
                //Console.WriteLine(q);
                SqlCommand cmd = new SqlCommand(q, conn);
                cmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine("exception caught" + e);
            }
        }

        static void Main(string[] args)
        {
            
            var twitter = new TwitterData
            {
                OAuthConsumerKey = "OAuth Consumer Key",
                OAuthConsumerSecret = "OAuth Consumer Secret"
            };

            int Choice=MainMenu();
            while (Choice <= 4)
            {
                switch (Choice)
                {
                    case 1:
                        Console.WriteLine("Enter User Name");
                        string Username = Console.ReadLine();
                        IEnumerable<string> twitts = twitter.GetTwitts(Username, 10).Result; //getting top 10 tweets of a particular user
                        foreach (var t in twitts)
                        {
                            Console.WriteLine(t + "\n");
                            add_UserTweets(Username,t);
                            Console.WriteLine("-------------------------------------------------------------------------");
                        }
                        Choice = MainMenu();
                        break;

                    case 2:
                        Console.WriteLine("Enter HashTag");
                        string HashTag = Console.ReadLine();
                        IEnumerable<string> Tweets_HashTag = twitter.GetTwitts_HashTags(HashTag).Result; //fetchng top 100 tweets related to a hash tag
                        foreach (var t in Tweets_HashTag)
                        {
                            Console.WriteLine(t + "\n");
                            add_HashTags(HashTag,t);
                            Console.WriteLine("-------------------------------------------------------------------------");
                        }
                        Choice = MainMenu();
                        break;

                    case 3:
                        IEnumerable<string> ReTtwitts = twitter.GetReTwitts("iphone6").Result;//getting the Retweet count of a particular tweet
                        List<string> Retweets = ReTtwitts.ToList();
                        int Retweet_count = Retweets.Count();

                        Console.WriteLine("ReTweet Count is : " + Retweet_count + "\n");
                        Console.WriteLine("-------------------------------------------------------------------------");
                        Choice = MainMenu();
                        break;
                    case 4:
                        Console.WriteLine("Enter Location Id of your place");
                        Console.WriteLine("Hint: Location id of the world is 1");
                        int Loc_id = int.Parse(Console.ReadLine());
                        IEnumerable<string> Trends = twitter.GetGlobalTrends(Loc_id).Result; //getting top 10 tweets of a particular user
                        foreach (var t in Trends)
                        {
                            Console.WriteLine(t + "\n");
                            add_Trends(Loc_id,t);
                            Console.WriteLine("-------------------------------------------------------------------------");
                        }
                        Choice = MainMenu();
                        break;

                }

            }
           
            Console.ReadKey();
        }
    }
}
